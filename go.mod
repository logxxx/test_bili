module kratos-demo1

go 1.13

require (
	github.com/go-kratos/kratos v0.5.1-0.20200526160825-521d240568d0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.4.2
	github.com/google/subcommands v1.2.0 // indirect
	github.com/google/wire v0.4.0
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/mod v0.3.0 // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e
	golang.org/x/tools v0.0.0-20200626032829-bcbc01e07a20 // indirect
	google.golang.org/genproto v0.0.0-20200626011028-ee7919e894b5
	google.golang.org/grpc v1.28.1
	google.golang.org/protobuf v1.25.0 // indirect
)
