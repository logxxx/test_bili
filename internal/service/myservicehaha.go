package service

import (
	"context"
	"fmt"
	"github.com/go-kratos/kratos/pkg/log"
	pb "kratos-demo1/api"
	"time"
)

func (s *Service) Haha(ctx context.Context, req *pb.HahaReq) (resp *pb.HahaResp, err error) {
	err = s.dao.GetThreads(ctx)
	if err != nil {
		log.Error("Haha GetThreads err:%v", err)
		return
	}
	resp = &pb.HahaResp{
		Result: fmt.Sprintf("%v, 现在时间是%v, 你疯了吧?",
			req.Name, time.Now().Format("2006/01/02 - 15:04:05")),
	}
	return
}


