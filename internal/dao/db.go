package dao

import (
	"context"
	"github.com/go-kratos/kratos/pkg/log"

	"kratos-demo1/internal/model"
	"github.com/go-kratos/kratos/pkg/conf/paladin"
	"github.com/go-kratos/kratos/pkg/database/sql"
)

func NewDB() (db *sql.DB, cf func(), err error) {
	var (
		cfg sql.Config
		ct paladin.TOML
	)
	if err = paladin.Get("db.toml").Unmarshal(&ct); err != nil {
		return
	}
	if err = ct.Get("Client").UnmarshalTOML(&cfg); err != nil {
		return
	}
	db = sql.NewMySQL(&cfg)
	cf = func() {db.Close()}
	return
}

func (d *dao) RawArticle(ctx context.Context, id int64) (art *model.Article, err error) {
	// get data from db
	return
}

func (d *dao) GetThreads(ctx context.Context) (error) {
	rows, err := d.db.Query(ctx, "select * from threads")
	if err != nil {
		return err
	}
	threads := make([]model.Thread, 0)
	for rows.Next() {
		thread := model.Thread{}
		err = rows.Scan(&thread.Id, &thread.Uuid, &thread.Topic, &thread.UserId, &thread.CreateAt)
		if err != nil {
			break
		}
		threads = append(threads, thread)
	}
	log.Info("GetThreads res:")
	for i := range threads {
		log.Info("%+v", threads[i])
	}
	return nil
}
