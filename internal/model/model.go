package model

import "time"

// Kratos hello kratos.
type Kratos struct {
	Hello string
}

type Article struct {
	ID int64
	Content string
	Author string
}

type Thread struct {
	Id int64 `db:"id"`
	Uuid string `db:"uuid"`
	Topic string `db:"topic"`
	UserId int `db:"user_id"`
	CreateAt time.Time `db:"created_at"`
}